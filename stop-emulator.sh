#!/usr/bin/env bash
#   Use this script to stop emulator
adb devices | grep emulator | cut -f1 | while read line; do adb -s $line emu kill; done &
chmod +x wait-for-it.sh && ./wait-for-it.sh -t 60 "OK: killing emulator, bye bye" -- echo "emulator is down" &
chmod +x wait-for-it.sh && ./wait-for-it.sh -t 60 "emulator: Saving state" -- echo "emulator is down"