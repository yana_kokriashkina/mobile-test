Feature: 3: Login positive

  Scenario: 1: Login with correct email Maxiru.TestUser1@gmail.com
    And User waits 5 seconds
    When User clicks "login|emailField"
    And User enters "Maxiru.TestUser1@gmail.com" into "login|emailField"
    And User press Enter
    And User clicks "login|passwordField"
    And User enters "1234" into "login|passwordField"
    And User press Enter
    And User waits 5 seconds
    Then "pin|pinInput" is displayed



