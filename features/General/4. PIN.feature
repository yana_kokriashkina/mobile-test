Feature: 4: Check PIN function

  Background: "CREW1" logs in
    When "CREW1" logs in

  Scenario: 1: Check elements on the screen
    Then "pin|pinInput" with text "Enter PIN" is displayed
    And Text "Create PIN" is displayed
    And Text "Please enter a 4 digits" is displayed
    And Text "This will be your universal PIN to access the modules" is displayed
    And "pin|button1" is displayed
    And "pin|button2" is displayed
    And "pin|button3" is displayed
    And "pin|button4" is displayed
    And "pin|button5" is displayed
    And "pin|button6" is displayed
    And "pin|button7" is displayed
    And "pin|button8" is displayed
    And "pin|button9" is displayed
    And "pin|button0" is displayed

  Scenario: 2: Press Next button with empty PIN field
    And User clicks "pin|buttonNext"
    Then Text "Please enter a 4 digits" is displayed

  Scenario: 3: Enter 5 symbols
    And User clicks "pin|button1"
    And User clicks "pin|button1"
    And User clicks "pin|button1"
    And User clicks "pin|button1"
    And User clicks "pin|button1"
    Then "pin|pinInput" with text "11111" is not displayed
    And "pin|pinInput" with text "1111" is displayed

  Scenario: 4: Clear and enter correct PIN
    And User clicks "pin|button1"
    And User clicks "pin|button2"
    And User clicks "pin|button3"
    And User clicks "pin|button4"
    And User clicks "pin|button5"
    Then "pin|pinInput" with text "12345" is not displayed
    And "pin|pinInput" with text "1234" is displayed
    When User clicks "pin|buttonBack"
    Then "pin|pinInput" with text "1234" is not displayed
    And "pin|pinInput" with text "123" is displayed
    When User clicks "pin|button4"
    And User clicks "pin|buttonNext"
    Then Text "Enter your PIN again for confirmation" is displayed

  Scenario: 5: Check elements on the confirmation screen
    And User clicks "pin|button1"
    And User clicks "pin|button2"
    And User clicks "pin|button3"
    And User clicks "pin|button4"
    And User clicks "pin|buttonNext"
    Then "pin|pinInput" with text "Enter PIN" is displayed
    And Text "Create PIN" is displayed
    And Text "Enter your PIN again for confirmation" is displayed
    And "pin|resetPINButton" is displayed
    And "pin|button1" is displayed
    And "pin|button2" is displayed
    And "pin|button3" is displayed
    And "pin|button4" is displayed
    And "pin|button5" is displayed
    And "pin|button6" is displayed
    And "pin|button7" is displayed
    And "pin|button8" is displayed
    And "pin|button9" is displayed
    And "pin|button0" is displayed
    And "pin|buttonNext" is displayed
    And "pin|buttonBack" is displayed

  Scenario: 6: Press Next button with empty PIN field
    And User clicks "pin|button1"
    And User clicks "pin|button2"
    And User clicks "pin|button3"
    And User clicks "pin|button4"
    And User clicks "pin|buttonNext"
    And User clicks "pin|buttonConfirmNext"
    And User waits 5 seconds
    Then Text "Wrong PIN entered" is displayed
    And Text "Please set your PIN again" is displayed
    And Text "TRY AGAIN" is displayed
    When User clicks "pin|tryAgainButton"
    And User waits 5 seconds
    Then Text "Wrong PIN entered" is not displayed

  Scenario: 7: Enter 5 symbols
    And User clicks "pin|button1"
    And User clicks "pin|button2"
    And User clicks "pin|button3"
    And User clicks "pin|button4"
    And User clicks "pin|buttonNext"
    And User clicks "pin|button1"
    And User clicks "pin|button1"
    And User clicks "pin|button1"
    And User clicks "pin|button1"
    And User clicks "pin|button1"
    Then "pin|pinInput" with text "11111" is not displayed
    And "pin|pinInput" with text "1111" is displayed

  Scenario: 8: Clear and enter correct PIN
    And User clicks "pin|button1"
    And User clicks "pin|button2"
    And User clicks "pin|button3"
    And User clicks "pin|button4"
    And User clicks "pin|buttonNext"
    And User clicks "pin|button1"
    And User clicks "pin|button2"
    And User clicks "pin|button3"
    And User clicks "pin|button4"
    And User clicks "pin|button5"
    Then "pin|pinInput" with text "12345" is not displayed
    And "pin|pinInput" with text "1234" is displayed
    When User clicks "pin|buttonConfirmBack"
    Then "pin|pinInput" with text "1234" is not displayed
    And "pin|pinInput" with text "123" is displayed
    When User clicks "pin|button3"
    And User clicks "pin|buttonConfirmNext"
    And User waits 5 seconds
    Then Text "Wrong PIN entered" is displayed
    And Text "Please set your PIN again" is displayed
    And Text "TRY AGAIN" is displayed
    When User clicks "pin|tryAgainButton"
    And User waits 5 seconds
    Then Text "Wrong PIN entered" is not displayed
    When User clicks "pin|button1"
    And User clicks "pin|button2"
    And User clicks "pin|button3"
    And User clicks "pin|button4"
    And User clicks "pin|buttonConfirmNext"
    And User waits 5 seconds
    Then Text "PIN created successfully" is displayed
    And Text "You’ll be directed to dashboard" is displayed
    And Text "OK" is displayed
    When User clicks "pin|okButton"
    And User waits 5 seconds
    Then Text "Dashboard" is displayed



