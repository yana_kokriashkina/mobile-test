Feature: 5: Crew application menu

  Background: "CREW1" logs in
    And User waits 5 seconds
    When "CREW1" logs in and creates PIN

  Scenario: 1: Menu open and close
    And User waits 5 seconds
    Then "menu|dashboardText" with text "Dashboard" is displayed
    And "menu|hamburger" is displayed
    When User clicks "menu|hamburger"
    And User waits 2 seconds
    And User performs swipe by x=100 from y=500 to y=0
    And User waits 2 seconds
    Then "menu|houskeeping" is displayed

  Scenario: 2: Check houskeeping menu item
    When User clicks "menu|hamburger"
    And User waits 2 seconds
    And User performs swipe by x=100 from y=500 to y=0
    And User waits 2 seconds
    And User clicks "menu|houskeeping"

  Scenario: 3: Check embarkation menu item
    When User clicks "menu|hamburger"
    And User waits 2 seconds
    And User performs swipe by x=100 from y=500 to y=0
    And User waits 2 seconds
    And User clicks "menu|greeterArrow"
    And User waits 2 seconds
    And User clicks "menu|embarkation"
    And User waits 2 seconds
    And User press Enter
    And User waits 2 seconds
    And User press Enter
    And User waits 2 seconds
    Then "embarkation|searchField" is displayed

  Scenario: 4: Check TR Admin menu item
    When User clicks "menu|hamburger"
    And User waits 2 seconds
    And User performs swipe by x=100 from y=500 to y=0
    And User waits 2 seconds
    And User clicks "menu|tableManagementArrow"
    And User waits 2 seconds
    And User clicks "menu|tableManagementAdmin"
    And User waits 2 seconds
    And User press Enter
    And User waits 2 seconds
    And User press Enter
    And User waits 2 seconds
    And User set "LANDSCAPE" orientation
    And User waits 2 seconds
    Then "trAdmin|restaurantDropDown" is displayed

  Scenario: 5: Check TR Host menu item
    When User clicks "menu|hamburger"
    And User waits 2 seconds
    And User performs swipe by x=100 from y=500 to y=0
    And User waits 2 seconds
    And User clicks "menu|tableManagementArrow"
    And User waits 2 seconds
    And User clicks "menu|tableManagementHost"
    And User waits 2 seconds
    And User press Enter
    And User waits 2 seconds
    And User press Enter
    And User waits 2 seconds
    And User set "LANDSCAPE" orientation
    And User waits 2 seconds
    Then "trHost|nearbyTab" is displayed
    And "trHost|reservationsTab" is displayed
    And "trHost|waitListTab" is displayed
    And "trHost|seatedTab" is displayed

  Scenario: 6: Check TR Reservations menu item
    When User clicks "menu|hamburger"
    And User waits 2 seconds
    And User performs swipe by x=100 from y=500 to y=0
    And User waits 2 seconds
    And User clicks "menu|tableManagementArrow"
    And User waits 2 seconds
    And User performs swipe by x=100 from y=500 to y=0
    And User waits 2 seconds
    And User clicks "menu|tableManagementReservations"
    And User waits 2 seconds
    And User press Enter
    And User waits 2 seconds
    And User press Enter
    And User waits 2 seconds
    And User set "LANDSCAPE" orientation
    And User waits 2 seconds
    Then "trReservations|header" with text "Reservations" is displayed

