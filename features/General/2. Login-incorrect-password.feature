Feature: 2: Login with incorrect password

  Scenario: 1: Login with incorrect password
    When User clicks "login|emailField"
    And User enters "Maxiru.TestUser1@gmail.com" into "login|emailField"
    And User press Enter
    And User clicks "login|passwordField"
    And User enters "12345" into "login|passwordField"
    And User press Enter
    And User waits 5 seconds
    Then "login|emailField" with text "Maxiru.TestUser1@gmail.com" is displayed
    And Text "Bad credentials" is displayed

  Scenario: 2: Login with empty password
    When User clicks "login|emailField"
    And User enters "Maxiru.TestUser1@gmail.com" into "login|emailField"
    And User press Enter
    And User clicks "login|passwordField"
    And User enters "" into "login|passwordField"
    And User press Enter
    And User waits 5 seconds
    Then "login|passwordField" is displayed
    And Text "Password is required" is displayed


