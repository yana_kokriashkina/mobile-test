Feature: 1: Login with incorrect email

  Scenario: 1: Login with incorrect email
    When User clicks "login|emailField"
    And User enters "Maxiru.TestUser@gmail.com" into "login|emailField"
    And User press Enter
    And User clicks "login|passwordField"
    And User enters "1234" into "login|passwordField"
    And User press Enter
    And User waits 5 seconds
    Then "login|emailField" with text "Maxiru.TestUser@gmail.com" is displayed
#    And Text "Bad credentials" is displayed

  Scenario: 2: Login with incorrect email format
    When User clicks "login|emailField"
    And User enters "Maxiru" into "login|emailField"
    And User press Enter
    And User clicks "login|passwordField"
    And User enters "1234" into "login|passwordField"
    And User press Enter
    And User waits 5 seconds
    Then "login|emailField" with text "Maxiru" is displayed
#    And Text "Bad credentials" is displayed

  Scenario: 3: Login with empty email
    When User clicks "login|emailField"
    And User enters "" into "login|emailField"
    And User press Enter
    And User waits 5 seconds
    Then "login|emailField" is displayed
#    And Text "Email is required" is displayed