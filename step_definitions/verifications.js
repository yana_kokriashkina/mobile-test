const { I } = inject();
const parameter = require ('../helpers/parameter-types');
const timeOut = process.env.TEST_TIMEOUT || 20;
const selectors = require('../data/locators');

Then('{string} is displayed', async(css) =>{
    let locator = await getLocator(css);
    I.seeElement(locator);
});

Then('{string} is not displayed', async(css) =>{
    let locator = await getLocator(css);
    I.dontSeeElement(locator);
});

Then('Text {string} is displayed', async(text) =>{
    text = await parameter.getText(text);
    I.seeElement("~"+text);
});

Then('Text {string} is not displayed', async(text) =>{
    text = await parameter.getText(text);
    I.dontSeeElement("~"+text);
});

Then('{string} activity is displayed', async(css) =>{
    css = await getLocator(css);
    I.seeCurrentActivityIs(css);
});

Then('{string} with text {string} is displayed', async(css, text) => {
    let locator = await getLocator(css);
    text = await parameter.getText(text);
    I.see(text, locator);
});

Then('{string} with text {string} is not displayed', async(css, text) => {
    let locator = await getLocator(css);
    text = await parameter.getText(text);
    I.dontSee(text, locator);
});

async function getLocator(locator, time = timeOut) {
    locator = await parameter.getLocator(locator);
    I.waitForElement(locator, time);
    return locator;
}