const {I} = inject();
const parameter = require('../helpers/parameter-types');
const timeOut = process.env.TEST_TIMEOUT || 120;
const selectors = require('../data/locators');


When('User clicks {string}', async (css) => {
    let locator = await getLocator(css);
    return await I.tap(locator);
});

When('User waits {int} seconds', async (sec) => {
    return await I.wait(sec);
});

When('User enters {string} into {string}', async (value, css) => {
    let locator = await getLocator(css);
    await I.fillField(locator, value);
    await I.hideDeviceKeyboard();
});

When('User press Enter', async () => {
    await I.sendDeviceKeyEvent(66);
});

When('User performs swipe by x={int} from y={int} to y={int}', async (x, yfrom, yto) => {
    await I.touchPerform([
        { action: 'longPress', options: { x: x, y: yfrom }},
        { action: 'moveTo', options: { x: x, y: yto }},
        { action: 'release' }
    ]);
});

When('User performs swipe by y={int} from x={int} to x={int}', async (y, xfrom, xto) => {
    await I.touchPerform([
        { action: 'longPress', options: { x: xfrom, y: y }},
        { action: 'moveTo', options: { x: xto, y: y }},
        { action: 'release' }
    ]);
});

When('User performs swipe from x={int}, y={int} to x={int}, y={int}', async (xfrom, yfrom, xto, yto) => {
    await I.touchPerform([
        { action: 'longPress', options: { x: xfrom, y: yfrom }},
        { action: 'moveTo', options: { x: xto, y: yto }},
        { action: 'release' }
    ]);
});

When('{string} logs in', async (role, time = timeOut) => {
    let user = await parameter.getUser(role);
    let emailField = selectors.login.emailField;
    let passwordField = selectors.login.passwordField;
    I.waitForElement(emailField, time);
    await I.tap(emailField);
    await I.fillField(emailField, user.username);
    await I.sendDeviceKeyEvent(66);
    I.waitForElement(passwordField, time);
    await I.tap(passwordField);
    await I.fillField(passwordField, user.password);
    return await I.sendDeviceKeyEvent(66);
});

When('{string} logs in and creates PIN', async (role, time = timeOut) => {
    let user = await parameter.getUser(role);
    let emailField = selectors.login.emailField;
    let passwordField = selectors.login.passwordField;
    let button1 = selectors.pin.button1;
    let buttonConfirm1 = selectors.pin.buttonConfirm1;
    let nextButton = selectors.pin.buttonNext;
    let nextConfirmButton = selectors.pin.buttonConfirmNext;
    let okButton = selectors.pin.okButton;

    I.waitForElement(emailField, time);
    await I.tap(emailField);
    await I.fillField(emailField, user.username);
    await I.sendDeviceKeyEvent(66);
    I.waitForElement(passwordField, time);
    await I.tap(passwordField);
    await I.fillField(passwordField, user.password);
    await I.sendDeviceKeyEvent(66);
    I.waitForElement(button1, time);
    await I.tap(button1);
    await I.tap(button1);
    await I.tap(button1);
    await I.tap(button1);
    await I.tap(nextButton);
    I.waitForElement(nextConfirmButton, time);
    await I.tap(buttonConfirm1);
    await I.tap(buttonConfirm1);
    await I.tap(buttonConfirm1);
    await I.tap(buttonConfirm1);
    await I.tap(nextConfirmButton);
    I.waitForElement(okButton, time);
    await I.tap(okButton);
});

When('User set {string} orientation', async (orientation) => {
    await I.setOrientation(orientation);
});

async function getLocator(locator, time = timeOut) {
    locator = await parameter.getLocator(locator);
    I.waitForElement(locator, time);
    return locator;
}
