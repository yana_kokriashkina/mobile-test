module.exports = {
    NCL: {
        QA: {
            CREW1:{
                username: "",
                password: ""
            }
        },
        QC: {
            CREW1:{
                username: "",
                password: ""
            }
        },
        DEV: {
            CREW1:{
                username: "",
                password: ""
            }
        }
    },
    DCL: {
        QA: {
            CREW1:{
                username: "",
                password: ""
            }
        },
        QC: {
            CREW1:{
                username: "",
                password: ""
            }
        },
        DEV: {
            CREW1:{
                username: "dcl_interface",
                password: "1Nt3rfac3"
            }
        }
    },
    DXP: {
        QA: {
            CREW1:{
                username: "Maxiru.TestUser1@gmail.com",
                password: "1234"
            }
        },
        QC: {
            CREW1:{
                username: "Maxiru.TestUser1@gmail.com",
                password: "1234"
            }
        },
        DEV: {
            CREW1:{
                username: "rosa.yang@gmail.com",
                password: "1234"
            }
        }
    },
    VV: {
        QA: {
            CREW1:{
                username: "",
                password: ""
            }
        },
        QC: {
            CREW1:{
                username: "michelle.williams@gmail.com",
                password: "1234"
            },
            CREW2:{
                username: "jacob.thomas@gmail.com",
                password: "1234"
            },
            CREW3:{
                username: "Hal.Borge247@gmail1.com",
                password: "1234"
            }
        },
        DEV: {
            CREW1:{
                username: "Hal.Borge247@gmail1.com",
                password: "1234"
            }
        }
    }
};