module.exports = {
    general:{
        view: "//android.view.View"
    },
    login:{
        emailField: "#emailInput",
        passwordField: "#currentPasswordInput",
        loginActivity: "MainActivity"
    },
    pin: {
        pinInput: "#pin",
        button1: '//android.view.View[@index="3"]/android.widget.Button[@index="0"]',
        button2: '//android.view.View[@index="4"]/android.widget.Button[@index="0"]',
        button3: '//android.view.View[@index="5"]/android.widget.Button[@index="0"]',
        button4: '//android.view.View[@index="6"]/android.widget.Button[@index="0"]',
        button5: '//android.view.View[@index="7"]/android.widget.Button[@index="0"]',
        button6: '//android.view.View[@index="8"]/android.widget.Button[@index="0"]',
        button7: '//android.view.View[@index="9"]/android.widget.Button[@index="0"]',
        button8: '//android.view.View[@index="10"]/android.widget.Button[@index="0"]',
        button9: '//android.view.View[@index="11"]/android.widget.Button[@index="0"]',
        button0: '//android.view.View[@index="13"]/android.widget.Button[@index="0"]',
        buttonBack: '//android.view.View[@index="12"]/android.widget.Button[@index="0"]',
        buttonNext: '//android.view.View[@index="14"]/android.widget.Button[@index="0"]',
        buttonConfirm1: '//android.view.View[@index="5"]/android.widget.Button[@index="0"]',
        buttonConfirmBack: '//android.view.View[@index="14"]/android.widget.Button[@index="0"]',
        buttonConfirmNext: '//android.view.View[@index="16"]/android.widget.Button[@index="0"]',
        resetPINButton: '~RESET PIN',
        tryAgainButton: '~TRY AGAIN',
        okButton: '//android.view.View[@index="1"]/android.view.View[@index="0"]/android.view.View[@index="0"]/android.widget.Button[@index="2"]',
    },
    menu: {
        dashboardText: '//android.view.View[@index="1"]/android.view.View[@index="0"]/android.view.View[@index="1"]',
        menu: '//android.view.View/android.view.View[@index="2"]/android.view.View[@index="0"]',
        hamburger: '//android.view.View[@index="1"]/android.view.View[@index="0"]/android.widget.Button[@index="0"]',
        houskeeping: '//android.view.View[@text="HOUSEKEEPING"]',
        greeter: '//android.view.View[@index="6"]/android.view.View[@text="GREETER"]',
        greeterArrow: '//android.view.View[@index="6"]/android.view.View[@index="1"]',
        embarkation: '//android.view.View[@index="6"]/android.view.View[@index="2"]/android.view.View[@index="1"]',
        dining: '//android.view.View[@index="7"]/android.view.View[@text="DINING"]',
        guestService: '//android.view.View[@index="8"]/android.view.View[@text="GUEST SERVICE"]',
        tableManagement: '//android.view.View[@index="9"]/android.view.View[@text="TABLE MANAGEMENT"]',
        tableManagementArrow: '//android.view.View[@index="9"]/android.view.View[@index="1"]',
        tableManagementAdmin: '//android.view.View[@index="9"]/android.view.View[@index="2"]',
        tableManagementHost: '//android.view.View[@index="9"]/android.view.View[@index="3"]',
        tableManagementReservations: '//android.view.View[@index="9"]/android.view.View[@index="4"]',
        crewPortal:'//android.view.View[@index="10"]/android.view.View[@text="CREW PORTAL"]',
        crewPortalArrow:'//android.view.View[@index="10"]/android.view.View[@text="icon-chevron-down"]',
        crewPortalMyProfile:'//android.view.View[@index="10"]/android.view.View[@text="MY PROFILE"]',
        crewPortalMyFolio:'//android.view.View[@index="10"]/android.view.View[@text="MY FOLIO"]',
        crewPortalPeople:'//android.view.View[@index="10"]/android.view.View[@text="PEOPLE"]',
        crewPortalTrackableManagement:'//android.view.View[@index="10"]/android.view.View[@text="TRACKABLE MANAGEMENT"]',
        logoutButton: '~LOGOUT',
    },
    embarkation: {
        searchField: '//android.widget.EditText[@index="1"]'
    },
    trAdmin: {
        restaurantDropDown: '#dropdown-venues-selected'
    },
    trHost: {
        nearbyTab: '//android.widget.ListView[@index="0"]/android.view.View[@index="0"]',
        reservationsTab: '//android.widget.ListView[@index="0"]/android.view.View[@index="1"]',
        waitListTab: '//android.widget.ListView[@index="0"]/android.view.View[@index="2"]',
        seatedTab: '//android.widget.ListView[@index="0"]/android.view.View[@index="3"]',
    },
    trReservations: {
        header: '//android.view.View[@index="0"]/android.view.View[@index="0"]/android.view.View[@index="0"]/android.view.View[@index="1"]',
    }
};