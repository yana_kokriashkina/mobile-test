const adb = require('./helpers/adb-helper');
const apk = 'DXP_QC_MOBILE.apk';
const wd = require('wd');
const deviceName = process.env.ANDROID_SERIAL;
    // ||"Device";
const port = process.env.ANDROID_ADB_SERVER_PORT;
    // ||5037;
const os = process.env.ANDROID_AVD_OS;
    // ||"8.1.0";

exports.config = {
    output: './output',
    name: 'mobile-test',
    helpers: {
        Appium: {
            platform: 'Android',
            automationName: 'UiAutomator2',
            autoLaunch: true,
            desiredCapabilities: {
                uiautomator2ServerLaunchTimeout:50000,
                uiautomator2ServerInstallTimeout: 50000,
                // app: apk,
                platformVersion: "8.0",
                deviceName: deviceName,
                adbPort: port,
                appActivity: 'com.decurtis.crew.MainActivity',
                appPackage: 'com.decurtis.crew',
            }
        },
        JSFailure: {
            require: './helpers/helper.js'
        }
    },
    gherkin: {
        features: './features/General/2. Login-incorrect-password.feature',
        steps: ['./step_definitions/actions.js', './step_definitions/verifications.js'],
    },
    bootstrap: async function (done) {
        await adb.install(apk);
        await adb.runApp();
        return await done();
    },

    teardown: async function (done) {
        await adb.unistall("io.appium.uiautomator2.server.test");
        await adb.unistall("io.appium.uiautomator2.server");
        await adb.unistall("io.appium.settings");
        await adb.unistall('com.decurtis.crew');
        return await done();
    },

    plugins: {
        screenshotOnFail: {
            enabled: true
        },
        allure: {
            enabled: true
        }
    }
};

