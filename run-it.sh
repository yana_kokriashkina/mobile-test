#!/usr/bin/env bash
#   Use this script to run tests
curl -O -J http://iana:1133edcefdc41b11259cebe73dea0f6dce@ci.simis.ai:8080/job/crew-dxp-qc-rn/lastSuccessfulBuild/artifact/apk-output/DXP_QC_MOBILE.apk
npm i
npm run appium &
# Wait for port 4723 to be listening connections
chmod +x wait-for-it.sh && ./wait-for-it.sh -t 60 0.0.0.0:4723 -- echo "driver is up"
npm run jenkins