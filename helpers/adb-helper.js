const Promise = require('bluebird');
const adb = require('adbkit');
const client = adb.createClient();
const request = require('request');
const Readable = require('stream').Readable;
let exec = require('child_process').exec;

module.exports = {

    install: async(apk) => {
        return await client.listDevices().then(function (devices) {
            return Promise.map(devices, function (device) {
                return client.install(device.id, apk);
            })
        }).then(function () {
            console.log('Applications %s were installed', apk)
        }).catch(function (err) {
            console.error('Something went wrong:', err.stack)
        });
    },

    start: async(activityNumber) => {
        return await client.listDevices().then(function (devices) {
            return Promise.map(devices, function (device) {
                return client.startActivity(device.id, activityNumber);
            })
        }).then(function () {
            console.log('Application was started')
        }).catch(function (err) {
            console.error('Something went wrong:', err.stack)
        });
    },

    unistall: async(pkg) => {
        return await client.listDevices().then(function (devices) {
            return Promise.map(devices, function (device) {
                return client.uninstall(device.id, pkg);
            })
        }).then(function () {
            console.log('Applications %s were deleted', pkg)
        }).catch(function (err) {
            console.error('Something went wrong:', err.stack)
        });
    },
    kill: async() => {
        return await client.kill().then(function () {
            return console.log('adb was killed');
        })
    },
    execShell: async(command) => {
        let { stdout } = await sh(command);
        for (let line of stdout.split('\n')) {
            console.log(`ls: ${line}`);
        }
    },

    runApp: async() => {
        let killApp = "adb shell am force-stop com.decurtis.crew";
        let runApp = "adb shell monkey -p com.decurtis.crew 1";
        let sleep = "sleep 2";

        try {
            let {stdout3} = await sh(runApp);
            for (let line of stdout3.split('\n')) {
                console.log(`test: ${line}`);
            }
        } catch (e) {

        }

        try {
            let {stdout2} = await sh(sleep);
            for (let line of stdout2.split('\n')) {
                console.log(`sleep: ${line}`);
            }
        } catch (e) {

        }

        try {
            let {stdout1} = await sh(killApp);
            for (let line of stdout1.split('\n')) {
                console.log(`test: ${line}`);
            }
        } catch (e) {

        }

        try {
            let {stdout2} = await sh(sleep);
            for (let line of stdout2.split('\n')) {
                console.log(`sleep: ${line}`);
            }
        } catch (e) {

        }

        try {
            let {stdout3} = await sh(runApp);
            for (let line of stdout3.split('\n')) {
                console.log(`test: ${line}`);
            }
        } catch (e) {

        }

    }

};
async function sh(cmd) {
    return new Promise(function (resolve, reject) {
        exec(cmd, (err, stdout, stderr) => {
            if (err) {
                reject(err);
            } else {
                resolve({ stdout, stderr });
            }
        });
    });
}