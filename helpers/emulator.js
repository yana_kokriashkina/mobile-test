const execa = require('execa');
const path = require('path');
const inquirer = require('inquirer');

const getAndroidHome = () => process.env.ANDROID_HOME || process.env.ANDROID_SDK;
const hasSdkPath = () => !!getAndroidHome();

const getEmulatorDir = () => path.join(getAndroidHome(), 'emulator');

const getExtendedPath = () => {
    const platformTools = path.join(getAndroidHome(), 'platform-tools');
    const emulator = getEmulatorDir();
    const tools = path.join(getAndroidHome(), 'tools');
    return `${process.env.PATH}:${tools}:${emulator}:${platformTools}`;
};

const run = async() => {
    const {stdout} = await execa('emulator', ['-list-avds'], {
        env: {PATH: getExtendedPath()},
    });
    return stdout.split('\n');
};

const startEmulator = async emulator => execa('./emulator', [`@${emulator}`], {
    detached: true,
    env: {PATH: getExtendedPath()},
    cwd: getEmulatorDir(),
});

module.exports = {
    hasSdkPath,
    run,
    startEmulator,
};

if (!hasSdkPath()) {
    console.error('Make sure that the ANDROID_SDK environment variable is set');
}

run().then(function () {
    console.log('🚀 Emulator is starting up...');
    startEmulator('Device');
    // process.exit();
}).catch((err) => {
    console.error(err);
    process.exit(1);
});