const selectors = require('../data/locators');
const userData = require('../data/users');
const sailorData = require('../data/sailors-list');
let environment = process.env.TEST_ENV || 'QC';
let ship = process.env.TEST_SHIP || 'DXP';

module.exports = {
    getLocator: async (s) => {
        if (s.indexOf('|') >= 0) {
            let dataArray = s.split('|');
            let component = dataArray[0];
            let value = dataArray[1];
            return selectors[component][value];
        } else {
            return s;
        }
    },

    getText: async (s) => {
        if (s.indexOf('sailor:') >= 0) {
            const array = s.split(':');
            const sailor = array[array.length - 1];
            return sailorData[ship][environment][sailor];
        } else {
            return s;
        }
    },
    getUser: async (role) => {
        return userData[ship][environment][role];
    }

};