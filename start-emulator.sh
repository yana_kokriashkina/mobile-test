#!/usr/bin/env bash
#   Use this script to run emulator
$ANDROID_HOME/tools/emulator -avd 1 &
chmod +x wait-for-it.sh && ./wait-for-it.sh -t 60 0.0.0.0:5037 -- echo "emulator is up"